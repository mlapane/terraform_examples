Content-Type: multipart/mixed; boundary="===============8168655305335414613=="
MIME-Version: 1.0

--===============8168655305335414613==
MIME-Version: 1.0
Content-Type: text/cloud-config; charset="us-ascii"
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud-config-amazon-ami.split"

#cloud-config
rsyslog:
  - content: |
      # rsyslog config stuff
      #################END CONFIG FILE#########################
    filename: 22-logfile.conf

ssh-authorized-keys:
  - # put the root user public key here (the default ssh key for the instance)

users:
  - default
  - name: user
    groups: whatever
    ssh-authorized-keys:
      - ssh-rsa key-material
    sudo: ALL=(ALL) NOPASSWD:ALL


package_upgrade: true

packages:
  - docker
  - ecs-init
  - aws-cli
  - awslogs
  - nfs-utils
  - jq
  - wget
  - bind-utils
  - rsyslog-gnutls
  - dnsmasq

runcmd:
  - docker ps
  - service docker stop
  - region = "${region}"
  - chkconfig docker on
  - [sh, -c, "usermod -a -G admin user"]
  - service docker start
  - start ecs
  - [sh, -c, "usermod -a -G docker user"]
  - echo ECS_CLUSTER=${cluster_name} >> /etc/ecs/ecs.config
  - echo ECS_ENGINE_AUTH_TYPE=dockercfg >> /etc/ecs/ecs.config
  - start ecs
  - service rsyslog restart
  - mkdir /etc/consul
  - mkdir -p /usr/local/tomcat/logs
  - mkdir -p /etc/rsyslog.d/keys/ca.d
  - service restart rsyslog
  - echo "prepend domain-name-servers 127.0.0.1;" >> /etc/dhcp/dhclient.conf
  - echo "server=/consul/127.0.0.1#8600" > /etc/dnsmasq.conf
  - echo "server=${aws_dns_ip}" >> /etc/dnsmasq.conf
  - echo "prepend domain-name-servers 127.0.0.1;" >> /etc/dhcp/dhclient.conf
  - sed -i 's/^nameserver.*$/nameserver 127.0.0.1/g' /etc/resolv.conf
  - chkconfig dnsmasq on
  - service dnsmasq restart


write_files:
  - content: |
      /var/log/cron
      /var/log/maillog
      /var/log/messages
      /var/log/secure
      /var/log/spooler
      {
      daily
      rotate 2
      compress
       sharedscripts
       postrotate
           /bin/kill -HUP `cat /var/run/syslogd.pid 2> /dev/null` 2> /dev/null || true
       endscript
      }
    path: "/etc/logrotate.d/syslog"
--===============8168655305335414613==
MIME-Version: 1.0
Content-Type: text/upstart-job; charset="us-ascii"
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="upstart-ecs.yml"

#upstart-job
description "Amazon EC2 Container Service (start task on instance boot)"
author "Amazon Web Services"
start on started ecs

script
  	exec 2>>/var/log/ecs/ecs-start-task.log
  	set -x
  	until curl -s http://localhost:51678/v1/metadata
  	do
  		sleep 1
  	done

  	# Grab the container instance ARN and AWS region from instance metadata
  	instance_arn=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .ContainerInstanceArn' | awk -F/ '{print $NF}' )
  	cluster_name=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .Cluster' | awk -F/ '{print $NF}' )
  	region=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .ContainerInstanceArn' | awk -F: '{print $4}')

  	# Specify the task definition to run at launch
  	dd_agent="dd-agent-task"
  	# Run the AWS CLI start-task command to start datadog on the container instance
  	 aws ecs start-task --cluster ${cluster_name} --task-definition $dd_agent --container-instances $instance_arn --started-by $instance_arn --region ${region}
end script
--===============8168655305335414613==
MIME-Version: 1.0
Content-Type: text/x-shellscript; charset="us-ascii"
Content-Transfer-Encoding: 7bit
#!/bin/bash
# Grab the container instance ARN and AWS region from instance metadata
instance_arn=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .ContainerInstanceArn' | awk -F/ '{print $NF}' )
cluster_name=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .Cluster' | awk -F/ '{print $NF}' )
region=$(curl -s http://localhost:51678/v1/metadata | jq -r '. | .ContainerInstanceArn' | awk -F: '{print $4}')

# Specify the task definition to run at launch
dd_agent="dd-agent-task"
# Run the AWS CLI start-task command to start datadog on the container instance
aws ecs start-task --cluster ${cluster_name} --task-definition $dd_agent --container-instances $instance_arn --started-by $instance_arn --region ${region}

docker pull gliderlabs/consul
docker pull gliderlabs/registrator
docker run -d --net=host -e 'CONSUL_LOCAL_CONFIG={"leave_on_terminate": true}' --restart=always -p 8301:8301 -p 8301:8301/udp -p 8400:8400 -p 8500:8500 -p 53:8600/udp \
        -v /opt/consul:/data -v /var/run/docker.sock:/var/run/docker.sock \
        -v /etc/consul:/etc/consul \
        -h $(curl -s http://169.254.169.254/latest/meta-data/instance-id) \
        --name consul-agent consul agent -join ${consul_server} -recursor=${aws_dns_ip} \
        -advertise $(curl -s http://169.254.169.254/latest/meta-data/local-ipv4) -datacenter ${consul_dc}

 docker run -d --net=host --restart=always -v /var/run/docker.sock:/tmp/docker.sock \
        -h $(curl -s http://169.254.169.254/latest/meta-data/instance-id) \
        --name consul-registrator gliderlabs/registrator:latest \
        -ip $(curl -s http://169.254.169.254/latest/meta-data/local-ipv4) \
        -tags "${environment}" \
        consul://localhost:8500
--===============8168655305335414613==--
