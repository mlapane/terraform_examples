resource "aws_route53_record" "someapp_elb" {
  zone_id = "${var.public_zone_id}"
  name    = "someapp.${var.public_zone_name}"
  type    = "A"

  alias {
    name                   = "${aws_elb.someapp_elb.dns_name}"
    zone_id                = "${aws_elb.someapp_elb.zone_id}"
    evaluate_target_health = true
  }
}
