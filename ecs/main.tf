/*Template config data*/
data "template_file" "cloud-config" {
  template = "${file("${path.module}/user_data/cloud-config.tpl")}"

  vars {
    dd_api_token  = "${var.dd_api_token}"
    cluster_name  = "${var.cluster_name}"
    ecs_version   = "latest"
    ecs_log_level = "info"
    region        = "${var.aws_region}"
    environment   = "${var.environment}"
    consul_server = "${element(split(",",var.consul_ips), 0)}"
    consul_dc     = "${var.region_name_short}-${var.environment}"
    aws_dns_ip    = "${var.aws_dns_ip}"
  }
}

resource "aws_ecs_cluster" "ecs-cluster" {
  name = "${var.cluster_name}"
}

resource "aws_launch_configuration" "project-ecs" {
  name_prefix                 = "project_ECS_${var.environment}_LC_"
  instance_type               = "${var.instance_type}"
  image_id                    = "${var.ami}"
  key_name                    = "${var.key_name}"
  user_data                   = "${data.template_file.cloud-config.rendered}"
  iam_instance_profile        = "${aws_iam_instance_profile.ecs.name}"
  associate_public_ip_address = false
  security_groups             = ["${split(",", var.security_groups)}"]

  ebs_block_device {
    device_name = "/dev/xvdcz"
    volume_size = 250
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "ecs" {
  name                      = "project_ECS_${var.environment}_ASG"
  launch_configuration      = "${aws_launch_configuration.project-ecs.name}"
  max_size                  = "${var.ecs_max_size}"
  min_size                  = "${var.ecs_min_size}"
  desired_capacity          = "${var.ecs_desired_size}"
  health_check_grace_period = 300
  health_check_type         = "${var.health_check_type}"
  load_balancers            = ["${aws_elb.someapp_elb.id}"]
  vpc_zone_identifier       = ["${split(",","${var.private_subnet_ids}")}"]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "project-ECS"
    propagate_at_launch = true
  }

  tag {
    key                 = "Role"
    value               = "project-ECS-Docker"
    propagate_at_launch = true
  }

  tag {
    key                 = "Terraform"
    value               = "true"
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = "${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Region"
    value               = "${var.region_name_short}"
    propagate_at_launch = true
  }
}
