[{
  "memory": 80,
  "name": "registrator",
  "cpu": 10,
  "image": "gliderlabs/registrator:latest",
  "essential": true,
  "command": ["consul://localhost:8500"],
  "logConfiguration": {
    "logDriver": "syslog"
  },
  "mountPoints": [{
    "containerPath": "/rootfs",
    "sourceVolume": "root",
    "readOnly": true
  }, {
    "containerPath": "/var/run",
    "sourceVolume": "var_run",
    "readOnly": false
  }, {
    "containerPath": "/sys",
    "sourceVolume": "sys",
    "readOnly": true
  }, {
    "containerPath": "/var/lib/docker",
    "sourceVolume": "var_lib_docker",
    "readOnly": true
  }, {
    "containerPath": "/tmp/docker.sock",
    "sourceVolume": "docker_socket",
    "readOnly": true
  }]
}]
