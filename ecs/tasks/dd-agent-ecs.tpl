[{
      "name": "dd-agent",
      "image": "datadog/docker-dd-agent:latest",
      "cpu": 10,
      "memory": 128,
      "essential": true,
      "dnsServers": [
        "172.17.0.1"
      ],
      "mountPoints": [
        {
          "containerPath": "/var/run/docker.sock",
          "sourceVolume": "docker_sock"
        },
        {
          "containerPath": "/host/sys/fs/cgroup",
          "sourceVolume": "cgroup",
          "readOnly": true
        },
        {
          "containerPath": "/host/proc",
          "sourceVolume": "proc",
          "readOnly": true
        },
        {
          "containerPath": "/conf.d",
          "sourceVolume": "dd-conf",
          "readOnly": true
        }
      ],
      "environment": [
        {
          "name": "API_KEY",
          "value": "${dd_api_token}"
        }
      ]
    }
  ]
