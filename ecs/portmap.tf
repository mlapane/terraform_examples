/* Container port maps */
variable "elbports" {
  default = {
    someapp = 443
  }
}

variable "container_ports" {
  default = {
    someapp = 12345
  }
}
