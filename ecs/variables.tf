/* BEGIN VARIABLES */
variable "server_count" {
  default = 3
}

variable "ami" {
  default = "ami-?????"
}

variable "private_subnet_ids" {}

variable "cluster_name" {}

variable "cidr_block" {}

variable "environment" {}

variable "instance_type" {
  default = "t2.large"
}

variable "connection_user" {
  default = "ec2-user"
}

variable "key_name" {
  default = "terraform"
}

variable "monitoring" {
  default = false
}

variable "az" {}

variable "aws_region" {}

variable "vpc_id" {}

variable "vpc_name" {}

variable "bastion_sg_id" {}

variable "bastion_pubip" {}

variable "dd_api_token" {}

variable "ecs_min_size" {}

variable "ecs_max_size" {}

variable "ecs_desired_size" {}

variable "public_subnet_ids" {}

variable "health_check_type" {
  default = "EC2"
}

variable "consul_ips" {}

variable "consul_dc" {}

variable "zone_name" {}

variable "public_someapp_sg" {}

variable "security_groups" {}

variable "region_name_short" {}

variable "aws_dns_ip" {}

variable "elb_settings" {
  default = {
    timeout  = "30"
    interval = "90"
  }
}

/* END VARIABLES */

