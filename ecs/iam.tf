#
# Create IAM Instance Profile
#
/* Render the policy file */
data "template_file" "ecs-task-policy" {
  template = "${file("${path.module}/iam/ecs-task-role-policy.json")}"

  vars {
    environment = "${var.environment}"
  }
}

/* Create the ecs iam role and policies */
resource "aws_iam_role" "ecs_iam_role" {
    name = "${var.vpc_name}_${var.environment}_ECS_Role"
    assume_role_policy = "${file("${path.module}/iam/ecs-role.json")}"
}

resource "aws_iam_role" "service_iam_role" {
  name = "${var.vpc_name}_${var.environment}_ECS_Service_Role"
  assume_role_policy = "${file("${path.module}/iam/ecs-service-role.json")}"
}

resource "aws_iam_role_policy" "ecs_iam_role" {
    name = "${var.vpc_name}_${var.environment}_ECS_Policy"
    role = "${aws_iam_role.ecs_iam_role.id}"
    policy = "${file("${path.module}/iam/ecs-role-policy.json")}"
  }

/* Create the ECS Task roles and policies */

resource "aws_iam_role" "ecs_task_role" {
  name = "${var.vpc_name}_${var.environment}_ECS_Task_Role"
  assume_role_policy = "${file("${path.module}/iam/ecs-task-role.json")}"
}

resource "aws_iam_role_policy" "ecs_task_role_policy" {
    name = "${var.vpc_name}_${var.environment}_ECS_Task_Policy"
    role = "${aws_iam_role.ecs_task_role.id}"
    policy = "${data.template_file.ecs-task-policy.rendered}"
  }


/*
ECS Service Scheduler Role - Needed to utilize the ELB updates
*/
resource "aws_iam_role_policy" "ecs_service_role_policy" {
  name     = "${var.vpc_name}_${var.environment}_ECS_Service_Role_Policy"
  policy   = "${file("${path.module}/iam/ecs-service-policy.json")}"
  role     = "${aws_iam_role.service_iam_role.id}"
}

/*
Attach IAM Profile to ASG Launch Configuration
*/
resource "aws_iam_instance_profile" "ecs" {
    name = "${var.vpc_name}_${var.environment}_ECS_Profile"
    roles = ["${aws_iam_role.ecs_iam_role.name}"]
}
