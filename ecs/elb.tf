/* Create the ELB for the Public Instances */
resource "aws_elb" "someapp_elb" {
  name                        = "project-someapp-${var.environment}-elb"
  connection_draining         = true
  connection_draining_timeout = 400
  security_groups             = ["${split(",",var.public_someapp_sg)}"]
  subnets                     = ["${split(",","${var.public_subnet_ids}")}"]

  listener {
    instance_port     = "${var.container_ports["someapp"]}"
    instance_protocol = "tcp"
    lb_port           = "${var.elbports["someapp"]}"
    lb_protocol       = "tcp"

    /*ssl_certificate_id = "arn:aws:acm:xxxxxxxxx"*/
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = "${var.elb_settings["timeout"]}"
    interval            = "${var.elb_settings["interval"]}"
    target              = "TCP:${var.container_ports["someapp"]}"
  }

  tags {
    Name        = "someapp_elb"
    Role        = "ELB"
    Environment = "${var.environment}"
  }
}
