resource "aws_security_group" "public_someapp" {
  description = "Public Side ELB someapp SG"
  vpc_id      = "${var.vpc_id}"

  tags {
    Name = "project-${var.environment}-sg_someapp_elbs"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 12345
    to_port     = 12345
    cidr_blocks = ["${var.cidr_block}"]
  }
}
