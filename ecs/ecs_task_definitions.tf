data "template_file" "dd" {
  template = "${file("${path.module}/tasks/dd-agent-ecs.tpl")}"

  vars {
    dd_api_token = "${var.dd_api_token}"
  }
}

data "template_file" "someapp" {
  template = "${file("${path.module}/tasks/someapp_task_definition.json")}"
}

data "template_file" "registrator" {
  template = "${file("${path.module}/tasks/registrator-task.tpl")}"

  vars {
    consul_server = "${var.consul_ip}"
  }
}

#
# Define tasks
#
resource "aws_ecs_task_definition" "datadog_agent" {
  network_mode = "bridge"
  family       = "dd-agent-task"

  volume {
    name      = "docker_sock"
    host_path = "/var/run/docker.sock"
  }

  volume {
    name      = "proc"
    host_path = "/proc"
  }

  volume {
    name      = "cgroup"
    host_path = "/cgroup/"
  }

  volume {
    name      = "dd-conf"
    host_path = "/opt/dd-agent-conf.d"
  }

  container_definitions = "${data.template_file.dd.rendered}"
}

resource "aws_ecs_task_definition" "registrator_task" {
  network_mode = "bridge"
  family       = "registrator-task"

  volume {
    name      = "docker_socket"
    host_path = "/var/run/docker.sock"
  }

  volume {
    name      = "var_lib_docker"
    host_path = "/var/lib/docker"
  }

  volume {
    name      = "sys"
    host_path = "/sys"
  }

  volume {
    name      = "var_run"
    host_path = "/var/run"
  }

  volume {
    name      = "root"
    host_path = "/"
  }

  container_definitions = "${data.template_file.registrator.rendered}"
}

resource "aws_ecs_task_definition" "someapp_task" {
  family                = "someapp"
  container_definitions = "${data.template_file.someapp.rendered}"
}
